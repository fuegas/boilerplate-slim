
module.exports = function(grunt) {
  grunt.initConfig({
    watch: {
      test: {
        files: [
          'src/**/*.*',
          'tests/**/*.*'
        ],
        tasks: ['clear','phplint','phpcs','phpunit']
      }
    },
    phpunit: {
      classes: {
        dir: 'tests/'
      },
      options: {
        bin: 'vendor/bin/phpunit',
        bootstrap: 'tests/bootstrap.php',
        configuration: 'phpunit.xml',
        noGlobalsBackup: true,
        color: true,
        followOutput: true,
        coverageHtml: 'coverage',
        testSuffix: '.test.php'
      }
    },
    phplint: {
      options: {
        swapPath: '/tmp'
      },
      all: [
        'src/**/*.php',
        'tests/**/*.php'
      ]
    },
    phpcs: {
      application: {
        src: [
          'src/**/*.php',
          'tests/**/*.php'
        ]
      },
      options: {
        standard: 'vendor/fuegas/vanilla-psr/ruleset.xml',
        extensions: 'php'
      }
    },
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-phpunit');
  grunt.loadNpmTasks('grunt-phplint');
  grunt.loadNpmTasks('grunt-phpcs');
  grunt.loadNpmTasks('grunt-clear');
};
