<?php

namespace Boilerplate;

/**
 * Environment class
 * Information about the environment in which the app runs.
 * Environments are for example "development", "staging" and "production".
 *
 * @author  Ferdi van der Werf <ferdi@savvii.nl>
 */
class Environment
{
  /**
   * Get environment variable.
   * If no environment has been set, we assume it is production.
   *
   * @return string Current environment
   */
  public static function get($envName = 'SLIM_ENV', $default = null)
  {
    return getenv($envName) ? getenv($envName) : $default;
  }
}
