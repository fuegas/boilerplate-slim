<?php

namespace Boilerplate;

/**
 * Config class
 * Class to get configuration values loaded from various ini files.
 * Different ini files can overwrite config values so it is important
 * to pay attention to order of loading.
 *
 * @author  Ferdi van der Werf <ferdi@savvii.nl>
 */
class Config
{
  /**
   * Static
   */

  /**
   * Instance of Config, for static access
   * @var Config
   */
  private static $instance;

  /**
   * Get the instance of Config.
   * If it does not exist, create it.
   * @return Config Instance of Config
   */
  private static function getInstance()
  {
    if (is_null(static::$instance)) {
      static::$instance = new Config();
    }
    return static::$instance;
  }

  /**
   * Load a config file (ini format).
   * @param  string $filename Config file to load
   */
  public static function loadConfig($filename)
  {
    static::getInstance()->loadIni($filename);
  }

  /**
   * Get a config value.
   *
   * If the value is in a section, use . to separate
   * section and key. For example: database.hostname
   *
   * @param  string $name    Name of the config value
   * @param  mixed  $default Default for the config
   * @return mixed        Config value
   */
  public static function config($name, $default = null)
  {
    return static::getInstance()->fetch($name, $default);
  }

  public static function params($paramsName, $default = null)
  {
    $app = \Slim\Slim::getInstance();
    $paramsValue = $app->request->params($paramsName);
    return ($paramsValue) ?: $default;
  }

  public static function paramsOrConfig($paramsName, $configName, $default = null)
  {
    $paramsValue = static::params($paramsName, $default);
    return is_null($paramsValue) ? static::config($configName, $default) : $paramsValue;
  }

  /**
   * Instance
   */

  /**
   * Array with config keys and values
   * @var Array
   */
  private $values;

  /**
   * Create an instance of Config.
   */
  private function __construct()
  {
    $this->values = [];
  }

  /**
   * Load ini file.
   * @param  string $filename Location of ini file
   */
  public function loadIni($filename)
  {
    if (!file_exists($filename)) {
      Log::debug("Could not load configuration file: '{$filename}' (does not exist)");
      return;
    }
    $config = $this->parseIniFile($filename, true);
    $this->values = $this->mergeRecursive($this->values, $config);
  }

  /**
   * Merge two array recursively and overwrite existing values.
   * Based on: http://php.net/manual/en/function.array-merge-recursive.php#92195
   * @param  array  &$array  Array to merge into
   * @param  array  &$array2 Array to merge from
   * @return array           Merged array
   */
  private function mergeRecursive(array &$array, array &$array2)
  {
    $merged = $array;

    foreach ($array2 as $key => &$value) {
      if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
        $merged[$key] = $this->mergeRecursive($merged[$key], $value);
      } else {
        $merged[$key] = $value;
      }
    }
    return $merged;
  }

  public function fetch($name, $default)
  {
    $parts = explode('.', $name);
    $array = &$this->values;
    foreach ($parts as $key) {
      if (!isset($array[$key])) {
        Log::debug("Requested config key not found: {$name}");
        return $default; // Config value not found, return default
      }
      $array = &$array[$key];
    }
    return $array;
  }

  /**
   * Proxy to stub during testing
   * @codeCoverageIgnore
   */
  protected function parseIniFile($filename, $processSections = false, $scannerMode = INI_SCANNER_NORMAL)
  {
    return parse_ini_file($filename, $processSections, $scannerMode);
  }
}
