<?php

namespace Boilerplate\Tests;

use \Boilerplate\Log;
use \Slim\Log as SlimLog;
use \Monolog\Logger as MonologLog;

/**
 * Class EnvironmentTest
 * @package Boilerplate\Tests
 */
class LogTest extends \PHPUnit_Framework_TestCase
{
  private $subject;

  public function setUp()
  {
    $this->subject = $this->getMock(
        'Boilerplate\Log',
        [ 'getLoggerResource' ]
    );
  }

  /**************************************************
   * Constructor
   **************************************************/

  public function testSettingsAreStored()
  {
    $subject = new Log([
      'FooBar' => 'Baz'
    ]);
    $this->assertEquals('Baz', $subject->getSetting('FooBar'));
  }


  public function testConstructorSetsNameByDefault()
  {
    $this->assertEquals(
        'SlimMonoLogger',
        $this->subject->getSetting('name')
    );
  }

  public function testConstructorSetsADefaultHandler()
  {
    $handlers = $this->subject->getSetting('handlers');
    $this->assertNotNull($handlers);
    $this->assertCount(1, $handlers);
    $this->assertInstanceOf(
        '\Monolog\Handler\StreamHandler',
        $handlers[0]
    );
  }

  public function testConstructorSetsEmptyProcessors()
  {
    $processors = $this->subject->getSetting('processors');
    $this->assertNotNull($processors);
    $this->assertCount(0, $processors);
  }

  /**************************************************
   * write
   **************************************************/

  public function testWriteCreatesResource()
  {
    $handlers = ['HANDLER'];
    $processors = ['PROCESSOR'];
    $mock = $this->getMock('\stdClass', ['pushHandler', 'pushProcessor', 'addRecord']);

    $mock->expects($this->once())
      ->method('pushHandler')
      ->with($handlers[0]);

    $mock->expects($this->once())
      ->method('pushProcessor')
      ->with($processors[0]);

    $mock->expects($this->once())
      ->method('addRecord');

    $this->subject = $this->getMock(
        '\Boilerplate\Log',
        [ 'getLoggerResource', 'getContext' ],
        [ [ 'name' => 'TestLogger', 'handlers' => $handlers, 'processors' => $processors ] ]
    );
    $this->subject->expects($this->once())
      ->method('getLoggerResource')
      ->with('TestLogger')
      ->willReturn($mock);
    $this->subject->expects($this->once())
      ->method('getContext')
      ->willReturn('CONTEXT');

    $this->subject->write('MSG', SlimLog::DEBUG);
  }

  /**************************************************
   * getLogLevel
   **************************************************/

  public function testGetLogLevelConvertsSlimLogLevel()
  {
    $this->assertEquals(
        MonologLog::DEBUG,
        $this->subject->getLogLevel(SlimLog::DEBUG, 0)
    );
  }

  public function testGetLogLevelDefaultsToGivenMonologLevel()
  {
    $this->assertEquals(
        MonologLog::EMERGENCY,
        $this->subject->getLogLevel(-1, MonologLog::EMERGENCY)
    );
  }

  /**************************************************
   * getContext
   **************************************************/

  public function testGetContextReturnsStringWithLastCall()
  {
    $this->assertContains(
        '/phpunit/Framework/TestCase.php',
        $this->subject->getContext()
    );
  }

  /**************************************************
   * getInstance
   **************************************************/

  public function testGetInstanceReturnsSetInstance()
  {
    $instance = 'NOT_A_REAL_INSTANCE';
    Log::$instance = $instance;
    $this->assertEquals($instance, $this->subject->getInstance());
    Log::$instance = null;
    unset($instance);
  }

  public function testGetInstanceWhenNotSet()
  {
    $this->assertInstanceOf(
        '\Boilerplate\Log',
        Log::getInstance()
    );
  }

  /**************************************************
   * Logging
   **************************************************/

  private function prepareInstanceForMessage($object, $level)
  {
    $this->subject = $this->getMock(
        '\Boilerplate\Log',
        [ 'write' ]
    );
    Log::$instance = $this->subject;
    $this->subject->expects($this->once())
      ->method('write')
      ->with($object, $level);
  }

  public function testLogCritical()
  {
    $this->prepareInstanceForMessage('CRITICAL_MSG', SlimLog::CRITICAL);
    Log::critical('CRITICAL_MSG');
  }

  public function testLogError()
  {
    $this->prepareInstanceForMessage('ERROR_MSG', SlimLog::ERROR);
    Log::error('ERROR_MSG');
  }

  public function testLogWarn()
  {
    $this->prepareInstanceForMessage('WARN_MSG', SlimLog::WARN);
    Log::warn('WARN_MSG');
  }

  public function testLogNotice()
  {
    $this->prepareInstanceForMessage('NOTICE_MSG', SlimLog::NOTICE);
    Log::notice('NOTICE_MSG');
  }

  public function testLogInfo()
  {
    $this->prepareInstanceForMessage('INFO_MSG', SlimLog::INFO);
    Log::info('INFO_MSG');
  }

  public function testLogDebug()
  {
    $this->prepareInstanceForMessage('DEBUG_MSG', SlimLog::DEBUG);
    Log::debug('DEBUG_MSG');
  }
}
