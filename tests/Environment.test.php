<?php

namespace Boilerplate\Tests;

use \Boilerplate\Environment;

/**
 * Class EnvironmentTest
 * @package Boilerplate\Tests
 */
class EnvironmentTest extends \PHPUnit_Framework_TestCase
{
  public function testDefault()
  {
    $this->assertEquals(null, Environment::get());
  }

  public function testEnvironmentSet()
  {
    // Get current environment
    $oldEnv = getenv('TEST_ENV');
    // Change environment
    putenv('TEST_ENV=FooBar');
    // Test code
    $this->assertEquals('FooBar', Environment::get('TEST_ENV'));
    // Reset environment
    putenv("TEST_ENV={$oldEnv}");
  }
}
