# Boilerplate for Slim applications

## Environment
You can get the environment the application is running in using the `Environment`
class. The default environment variable that is requested is SLIM_ENV. If you want
to use a different variable you can specify it when requesting the environment
value:

````php
Environment::env() // Returns SLIM_ENV value or default (null)
Environment::env('MY_ENV') // Returns MY_ENV value or default (null)
Environment::env('MY_ENV', 'production') // Returns MY_ENV value or default ('production')
````

## Logger

The boilerplate has Monolog as a dependency. It can be injected into Slim
during instantiation.

By default the logger will be injected with the following values:
````php
array(
  'name' => 'SlimMonoLogger',
  'handlers' => array(
    new \Monolog\Handler\StreamHandler('./logs/'.date('y-m-d').'.log'),
  ),
  'processors' => array(),
)
````

If you want to use `stdout` as logger, use the following instance call:
````php
Log::getInstance([
  'name' => 'YourLogger',
  'handlers' => [ new \Monolog\Handler\StreamHandler('php://stdout') ]
]);
````

For other logging capabilities, please take a look at:
[https://github.com/Seldaek/monolog](https://github.com/Seldaek/monolog)

If you want to inject the logger into Slim yourself see the following example:
````php
$app = new \Slim\Slim(array(
  'log.writer' => new Ssllii\Helpers\Log(array(
    'name' => 'SlimMonoLogger',
    'handlers' => array(
      new \Monolog\Handler\StreamHandler('./logs/'.date('Y-m-d').'.log'),
    ),
    'processors' => array(
      function ($record) {
        $record['extra']['dummy'] = 'Hello world!';
        return $record;
      },
    ),
  ))
));
````
